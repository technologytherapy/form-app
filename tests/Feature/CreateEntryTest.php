<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\{Form, Entry};

class CreateValidEntryTest extends TestCase
{

    /*
    |--------------------------------------------------------------------------
    | Create Valid Entry Test
    |--------------------------------------------------------------------------
    |
    | This file tests the validity of an example form entry.
    | It asserts that the api response is correct and
    | that db entries are created when relevant.
    */

    use WithoutMiddleware;
    use DatabaseTransactions;


    public function testValidEntryIsCreated()
    {
        $form = Form::create([
            'name' => 'Internal Test Form',
            'url' => config('app.url'),
            'token' => 'sdefasfs'
        ]);

        $response = $this->post('/api/entries', [
            'booking_first_name_0' => 'James',
            'site' => config('app.url'),
            'first_name' => 'Zack',
            'last_name' => 'Dude',
            'token' => 'sdefasfs'
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('entries', [
            'id' => $response->json()['id']
        ]);
    }

    public function testInvalidTokenFails()
    {
        $form = Form::create([
            'name' => 'Internal Test Form',
            'url' => config('app.url'),
            'token' => 'sdefasfs'
        ]);

        $response = $this->post('/api/entries', [
            'booking_first_name_0' => 'James',
            'site' => config('app.url'),
            'first_name' => 'Zack',
            'last_name' => 'Dude',
            'token' => 'wqresrhdtjyrkdj'
        ]);

        $response->assertStatus(500);
    }

    public function testInvalidUrlFails()
    {
        $form = Form::create([
            'name' => 'Internal Test Form',
            'url' => config('app.url'),
            'token' => 'sdefasfs'
        ]);

        $response = $this->post('/api/entries', [
            'booking_first_name_0' => 'James',
            'site' => 'wrong site',
            'first_name' => 'Zack',
            'last_name' => 'Dude',
            'token' => 'wqresrhdtjyrkdj'
        ]);

        $response->assertStatus(500);
    }
}
