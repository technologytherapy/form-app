<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationRule extends Model
{
    protected $fillable = ['id', 'form_id', 'recipients', 'subject', 'type', 'token'];

    protected $casts = ['recipients' => 'array'];

    /**
     * Get the form the notifications belong to
     */
    public function notificationRules()
    {
        return $this->belongsTo('App\Form');
    }
}
