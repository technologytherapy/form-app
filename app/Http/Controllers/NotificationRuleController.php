<?php

namespace App\Http\Controllers;

use App\NotificationRule;
use Illuminate\Http\Request;

use App\Form;

class NotificationRuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'recipients' => 'email_array',
            'form_id' => 'required|exists:forms,id'
        ]);

        $form = Form::find($request->input('form_id'));

        $emailNotificationRule = NotificationRule::updateOrCreate(
            [
                'form_id' => $request->input('form_id'),
                'type' => 'email',
                'subject' => 'New Submission from ' . $form->name
            ],
            [
                'recipients' => array_map('trim', explode(',', $request->input('recipients'))),
            ]
        );

        return redirect()->route('forms.show', [$form]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NotificationRule  $notificationRule
     * @return \Illuminate\Http\Response
     */
    public function show(NotificationRule $notificationRule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NotificationRule  $notificationRule
     * @return \Illuminate\Http\Response
     */
    public function edit(NotificationRule $notificationRule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NotificationRule  $notificationRule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NotificationRule $notificationRule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NotificationRule  $notificationRule
     * @return \Illuminate\Http\Response
     */
    public function destroy(NotificationRule $notificationRule)
    {
        //
    }
}
