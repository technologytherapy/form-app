<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\{Form, Entry};

class HomeController extends Controller
{
    public function index()
    {
        return view('home', [
            'forms' => Form::all(),
            'entries' => Entry::all()
        ]);
    }
}
