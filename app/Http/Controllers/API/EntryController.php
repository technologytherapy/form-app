<?php

namespace App\Http\Controllers\API;

use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\EntryCreated;

use App\{Entry, Form};

class EntryController extends Controller
{
    /**
     * Lists all form entries.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Entry::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = Form::matches($request->input('token'), $request->headers->get('origin'))->first();

        // If the request url isn't approved, return a json error
        if (!$form) {
            return response()->json([
                'error' => 'Entry is invalid'
            ], 500);
        };

        // Create the form entry
        $entry = Entry::create([
            'form_id' => $form->id,
            'data' => $request->except(['token'])
        ]);

        // Notify the appropriate users
        $form->notify(new EntryCreated($entry));

        return $entry;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
