<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['site', 'data', 'form_id'];

    /**
     * The attributes that are cast to arrays.
     *
     * @var array
     */
    protected $casts = ['data' => 'array'];

    /**
     * Get the form that owns the entry
     */
    public function form()
    {
        return $this->belongsTo('App\Form');
    }
}
