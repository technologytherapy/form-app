<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    use Notifiable;

    protected $fillable = ['id', 'name', 'url', 'token'];

    /**
     * Route notifications for the mail channel.
     *
     * @return string
     */
    public function routeNotificationForMail()
    {
        if (!$this->emailNotificationRecipients()) {
            return 'zack@technologytherapy.com';
        }

        return collect($this->emailNotificationRecipients())->map(function ($item) {
            return [
                'name' => $item,
                'email' => $item
            ];
        });
    }

    /**
     * Get the entries for the Form
     */
    public function entries()
    {
        return $this->hasMany('App\Entry');
    }

    /**
     * Get the notification rules for the form
     */
    public function notificationRules()
    {
        return $this->hasMany('App\NotificationRule');
    }

    /**
     * Get the single email notification recipients for the form
     */
    public function emailNotificationRecipients()
    {
        $emailNotifications = $this->notificationRules->where('type', 'email')->first();

        if ($emailNotifications && $emailNotifications->recipients) {
            return $emailNotifications->recipients;
        }
    }


    /**
     * Scope a query to check forms by token and id.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMatches($query, $token, $url)
    {
        return $query->where([
            ['token', '=', $token],
            ['url', '=', $url]
        ]);
    }
}
