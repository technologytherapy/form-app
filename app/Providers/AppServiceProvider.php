<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // Email array validator
        Validator::extend('email_array', function ($attribute, $value, $parameters, $validator) {

            // Convert the string to an array format Laravel can use
            $value = str_replace(' ', '', $value);
            $array = explode(',', $value);

            foreach ($array as $email) {
                $email_to_validate['email_array'][] = $email;
            }

            $rules = array('email_array.*' => 'email');
            $messages = array(
                 'email_array.*.email' => 'Must be a valid, comma-seperated list of emails'
            );

            $validator = Validator::make($email_to_validate, $rules, $messages);

            if ($validator->passes()) {
                return true;
            } else {
                return false;
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
