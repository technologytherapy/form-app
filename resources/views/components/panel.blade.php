<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">

         @if($title)
         <div class="panel-heading">
            {{ $title }}
         </div>
         @endif

         <div class="panel-body">
            {{ $slot }}
        </div>

    </div>
</div>