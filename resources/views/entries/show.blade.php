@extends('layouts.app')

@section('content')

@component('components.panel')
    @slot('title')
        View Entry: {{ $entry->id }}
    @endslot

    <div class="pull-right">
        <a role="button" class="btn btn-primary" href="{{ route('forms.show', $entry->form) }}">Back to Form</a>
    </div>

    @if(isset($entry->data))
        <h2 class="h4">Entry Data</h2>
        <hr>
        <ul>
        @foreach($entry->data as $key => $value)
        <li><strong>{{ $key }}</strong> : {{ $value }}</li>
        @endforeach
        </ul>
    @endif

    <hr>
@endcomponent

@endsection