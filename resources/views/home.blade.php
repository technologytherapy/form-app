@extends('layouts.app')

@section('content')

@component('components.panel')
    @slot('title')
    Forms
    @endslot

    <div class="clearfix">
        <a role="button" class="btn btn-primary pull-right" href="{{ route('forms.create') }}">Add New</a>
    </div>

    @if(isset($forms) && count($forms) > 0)
        @include('tables.forms')
    @else
        <p>No forms yet! Why not <a href="{{ route('forms.create') }}">Create a form?</a>
    @endif
@endcomponent

@component('components.panel')
    @slot('title')
    Recent Entries
    @endslot

    @if(isset($entries) && count($entries) > 0)
        @include('tables.entries')
    @else
        <p>No form entries yet!
        @if(isset($forms) && count($forms) <= 0)
            Why not <a href="{{ route('forms.create') }}">Create a form?</a>
        @elseif(isset($forms) && count($forms) > 0)
            It won't be long until someone fills out your forms! Check back later.
        @endif
    @endif
@endcomponent

@endsection
