@extends('layouts.app')

@section('content')

@component('components.panel')
    @slot('title')
        View Form: {{ $form->name }}
    @endslot

    <div class="pull-right">
        <a role="button" class="btn btn-primary" href="{{ route('forms.edit', ['id' => $form->id]) }}">Edit Form</a>
        <a role="button" class="btn btn-danger" href="{{ route('home') }}">Back</a>
    </div>

    <h2 class="h5">URL</h2>
    <p>Be sure your form is served from:<br><code>{{ $form->url }}</code></p>

    <hr>
    <h2 class="h5">Token Field (Required)</h2>
    <p>Be sure your form includes the following field:<br><code>{{ '<input name="token" value="' . $form->token . '">' }}</code></p>

    <hr>
    <h2 class="h5">Notifications</h2>
    <p>Each email address listed here wil receive an email every time a user submits your form. Please provide a comma seperated list of recipients.</p>

    <form action="{{ route('notification-rules.store') }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('POST') }}

        <div class="form-group{{ $errors->has('recipients') ? ' has-error' : '' }}">
            <label for="name">Recipients</label>
            <input class="form-control" type="text" placeholder="email@website.com, fake@yourwebsite.com" name="recipients"
            @if($recipients && count($recipients > 0))
                    value="{{ implode(', ', $recipients) }}"
            @endif required>
            @if ($errors->has('recipients'))
                <span class="help-block">
                    <strong>{{ $errors->first('recipients') }}</strong>
                </span>
            @endif
        </div>

        <input type="hidden" name="form_id" value="{{ $form->id }}" required>
        <button class="btn btn-success" type="submit">Save Recipients</button>
    </form>
@endcomponent

@component('components.panel')
    @slot('title')
        Entries
    @endslot

    @if(isset($entries) && count($entries) > 0)
        @include('tables.entries')
    @else
        <p>It won't be long until someone fills out your form! Check back later.</p>
    @endif
@endcomponent

@endsection