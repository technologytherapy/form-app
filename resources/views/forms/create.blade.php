@extends('layouts.app')

@section('content')

@component('components.panel')
    @slot('title')
        Create a Form
    @endslot


    <div class="clearfix">
        <a role="button" class="btn btn-danger pull-right" href="{{ route('home') }}">Back</a>
    </div>


    <form action="{{ route('forms.store') }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('POST') }}

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">Form Name</label>
            <input class="form-control" type="text" placeholder="Name" name="name" value="{{ old('name') }}" required>
            @if ($errors->has('url'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
            <label for="url">Form URL/Permalink</label>
            <input class="form-control" type="text" placeholder="Url" name="url" value="{{ old('url') }}" required>
            <span class="help-block">This should be the base url of your site, for example <code>http://mysite.com</code>.</span>
            @if ($errors->has('url'))
                <span class="help-block">
                    <strong>{{ $errors->first('url') }}</strong>
                </span>
            @endif
        </div>
        <button class="btn btn-success" type="submit">Save Form</button>
    </form>
@endcomponent

@endsection
