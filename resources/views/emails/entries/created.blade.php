@component('mail::message')
# New entry for {{ $entry->form->name }}

On {{ $entry->created_at->toDayDateTimeString() }} a user filled out your form.


@component('mail::panel')
## Data

@foreach($entry->data as $key => $value)
- **{{ $key }}** : {{ $value }}
@endforeach
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
