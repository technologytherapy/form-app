<table class="table">
    <thead>
        <th>ID</th>
        <th>Form</th>
        <th>Created At</th>
        {{-- <th>Data</th> --}}
        <th>{{-- Edit --}}</th>
    </thead>
    <tbody>
    @foreach($entries as $entry)
        <tr>
            <td>{{ $entry->id }}</td>
            <td>{{ $entry->form->name }}</td>
            <td>{{ $entry->created_at->diffForHumans() }}</td>
            {{-- @if(isset($entry->data))
                <td>
                @foreach($entry->data as $key => $value)
                <strong>{{ $key }}</strong> : {{ $value }}
                @endforeach
                </td>
            @endif --}}
            <td><a href="{{ url('/entries/' . $entry->id) }}">View <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></td>
        </tr>
    @endforeach
    </tbody>
</table>