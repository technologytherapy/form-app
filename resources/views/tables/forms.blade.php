<table class="table">
    <thead>
        <th>ID</th>
        <th>Name</th>
        <th>Entries</th>
        <th>{{-- Edit --}}</th>
    </thead>
    <tbody>
    @foreach($forms as $form)
        <tr>
            <td>{{ $form->id }}</td>
            <td>{{ $form->name }}</td>
            <td>{{ $form->entries->count() }}</td>
            <td><a href="{{ url('/forms/' . $form->id) }}">View <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></td>
        </tr>
    @endforeach
    </tbody>
</table>