<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Approved Sites
    |--------------------------------------------------------------------------
    |
    | This value is the list of approved sites that can submit entries.
    */

    'approved_sites' => explode(',', env('APPROVED_SITES'))
];
