<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Public Controllers
Route::get('/', 'SplashController@index');

// @TODO: Add user roles and limit this to 'admin' users
// Administrative Controllers
Route::get('home', 'HomeController@index')->name('home');

Route::resource('forms', 'FormController');
Route::resource('notification-rules', 'NotificationRuleController');

Route::resource('entries', 'EntryController', [
    'except' => ['create']
]);
